package com.hoteles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebGestionHotelesApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebGestionHotelesApplication.class, args);
	}

}
