import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

export interface Hotels {
  nombre: string;
  viewNombre: string;
}
export interface Estrellas {
  nombre: string;
  viewNombre: string;
}
@Component({
  selector: 'app-busqueda-habitacion',
  templateUrl: './busqueda-habitacion.component.html',
  styleUrls: ['./busqueda-habitacion.component.css']
})
export class BusquedaHabitacionComponent implements OnInit {
  minDate = new Date();
  maxDate = new Date(2030, 3, 10);
  fromDate: any;
  toDate: any;

  hoteles: Hotels[] = [
    { nombre: 'nombre-0', viewNombre: 'Gran Meliá Palacio de los Duques' },
    { nombre: 'nombre-1', viewNombre: 'AC Palacio Del Retiro' },
    { nombre: 'nombre-2', viewNombre: 'Hotel Villa Magna' }
  ];

  estrellas: Estrellas[] = [
    { nombre: 'nombre-0', viewNombre: '****' },
    { nombre: 'nombre-1', viewNombre: '***' },
    { nombre: 'nombre-2', viewNombre: '**' }
  ];

  toppings = new FormControl();
  toppingList: string[] = ['Viajero Solo', 'Pareja sin Hijos', 'Pareja con Hijos', 'Grupos', 'Negocios'];
  events: string[] = [];

  constructor() { }

  ngOnInit() {
  }

  recogerFechaEntrada(type: string, event: MatDatepickerInputEvent<Date>) {
    this.events.push(`${event.value}`);
    this.fromDate =  this.events;
    alert(this.fromDate);
    this.resetForm();
  }

  recogerFechaSalida(type: string, event: MatDatepickerInputEvent<Date>) {
    this.events.push(`${event.value}`);
    this.toDate = this.events;
    alert(this.toDate);
    this.resetForm();
  }

  resetForm() {
    this.fromDate = undefined;
    this.toDate = undefined;
  };

  getTipoHabitacion(topping: any) {

  }
  getTipoHotel(hotel: any) {
    alert(hotel);
  }

  getHotel(hotel: any) {
    alert(hotel);
  }

  aceptar() {
    alert("prueba");
  }
}
