import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErrorCargarPaginaComponent } from './components/error-cargar-pagina/error-cargar-pagina.component';
import { HomeComponent } from './components/home/home.component';

//Arrays de rutas
const routes: Routes = [
	{path: '', component: HomeComponent},

  {path: '**', component: ErrorCargarPaginaComponent} //cargar siempre el último para que no falle
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
