import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { ErrorCargarPaginaComponent } from './components/error-cargar-pagina/error-cargar-pagina.component';
import { BusquedaHabitacionComponent } from './components/busqueda-habitacion/busqueda-habitacion.component';


import { HeaderComponent } from './components/header/header.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import {
  MatTooltipModule, MatToolbarModule, MatSidenavModule, MatIconModule,
  MatListModule, MatMenuModule, MatNativeDateModule, MatInputModule,
  MatTabsModule, MatSlideToggle, MatSliderModule, MatFormFieldModule
} from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { PopUpComponent } from './components/pop-up/pop-up.component';
import { SliderComponent } from './components/slider/slider.component';
import { SliderDirective } from './components/slider/slider.directive';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HomeComponent,
    ErrorCargarPaginaComponent,
    HeaderComponent,
    BusquedaHabitacionComponent,
    SliderComponent,
    SliderDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule, HttpClientModule,
    MatSidenavModule, MatCardModule, MatFormFieldModule,
    MatIconModule, MatTooltipModule, MatMenuModule, MatSliderModule,
    MatDatepickerModule, MatInputModule, MatTabsModule, ReactiveFormsModule,
    MatListModule, MatSelectModule, FormsModule, MatNativeDateModule
  ],
  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule { }
