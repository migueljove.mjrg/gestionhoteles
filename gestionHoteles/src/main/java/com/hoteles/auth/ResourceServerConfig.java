package com.hoteles.auth;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	private static final String[] AUTH_WHITELIST = {
			// -- swagger ui
			"/swagger-resources/**", "/swagger-ui.html", "/v2/api-docs", "/webjars/**" };

	@Override
	public void configure(HttpSecurity http) throws Exception {

		// PARA PRUEBAS -> DESHABILITA LA SEGURIDAD DE LA API
		//
		http.authorizeRequests().anyRequest().permitAll();

		// HABILITAR SEGURIDAD
		//
		// http.authorizeRequests()
		// .antMatchers(AUTH_WHITELIST).permitAll()
		// .antMatchers(HttpMethod.GET, "/hotel").hasRole("USER")
		// .antMatchers(HttpMethod.GET, "/hotel/**").hasRole("USER")
		// .antMatchers(HttpMethod.GET).hasRole("ADMIN")
		// .antMatchers(HttpMethod.DELETE).hasRole("ADMIN")
		// .antMatchers(HttpMethod.POST).hasRole("ADMIN")
		// .antMatchers(HttpMethod.PUT).hasRole("ADMIN")
		// .anyRequest().authenticated();
	}

}
