package com.hoteles.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.hoteles.dao.ClienteDao;
import com.hoteles.dao.entitys.Cliente;
import com.hoteles.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService, UserDetailsService {

	@Autowired
	private ClienteDao clientedao;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Cliente cliente = clientedao.getClienteByEmail(email);
		List<String> roles = null;

		if (!cliente.getCli_nombre().equalsIgnoreCase("admin")) {
			roles = new ArrayList<>();
			roles.add("ROLE_USER");
		} else {
			roles = new ArrayList<>();
			roles.add("ROLE_USER");
			roles.add("ROLE_ADMIN");
		}

		List<GrantedAuthority> authorities = roles.stream().map(role -> new SimpleGrantedAuthority(role))
				.collect(Collectors.toList());

		return new User(cliente.getCli_email(), cliente.getPassword(), true, true, true, true, authorities);
	}

}
